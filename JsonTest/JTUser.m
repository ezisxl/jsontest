//
//  JTUser.m
//  JsonTest
//
//  Created by Ansis on 21/08/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import "JTUser.h"

@implementation JTUser

- (id)initWithDictionary:(NSDictionary *)userJsonData
{
    int uid = [userJsonData[@"id"] intValue];
    _uId = [NSNumber numberWithInt: uid];
    _first_name = userJsonData[@"first_name"];
    _last_name = [userJsonData objectForKey:@"last_name"]; // tas pats kas userJsonData[@"last_name"]
    _country = userJsonData[@"country"];
    _ip_address = userJsonData[@"ip_address"];
    _email = userJsonData[@"email"];

    return self;
    
}

- (NSString*)fullName
{
    NSString *nameAndSurname = [NSString stringWithFormat:@"name: %@ surname: %@", _first_name, _last_name];
    
    return nameAndSurname;
}

- (NSString*)cleanName
{
    NSString *nameAndSurname = [NSString stringWithFormat:@"%@ %@", _first_name, _last_name];
    
    return nameAndSurname;
    
}
@end
