//
//  main.m
//  JsonTest
//
//  Created by Ansis on 21/08/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JTAppDelegate class]));
    }
}
