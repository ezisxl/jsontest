//
//  JTUserTableViewCell.h
//  JsonTest
//
//  Created by Ansis on 21/08/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JTUser;

@interface JTUserTableViewCell : UITableViewCell

- (void)updateCellWithModel:(JTUser*)userModel;


@end
