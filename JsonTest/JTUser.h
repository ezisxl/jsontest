//
//  JTUser.h
//  JsonTest
//
//  Created by Ansis on 21/08/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JTUser : NSObject

- (id)initWithDictionary:(NSDictionary*)userJsonData;
- (NSString*)fullName;
- (NSString*)cleanName;

@property (nonatomic, strong) NSNumber *uId;
@property (nonatomic, strong) NSString *first_name;
@property (nonatomic, strong) NSString *last_name;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *ip_address;
@property (nonatomic, strong) NSString *email;

@end
