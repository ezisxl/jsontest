//
//  JTUserTableViewCell.m
//  JsonTest
//
//  Created by Ansis on 21/08/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import "JTUserTableViewCell.h"

//models
#import "JTUser.h"

@interface JTUserTableViewCell ()

@property (nonatomic, weak) JTUser *pUser;

@property (nonatomic, strong) UILabel *pIdLabel;
@end

@implementation JTUserTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        _pIdLabel = [[UILabel alloc] initWithFrame: CGRectMake(self.frame.size.width - 50, 0, 30, 0)];
        [_pIdLabel setTextColor: [UIColor whiteColor]];
        [_pIdLabel setBackgroundColor: [UIColor blueColor]];
        [_pIdLabel setTextAlignment: NSTextAlignmentCenter];
        [_pIdLabel setFont: [UIFont boldSystemFontOfSize: 14]];

        [self.contentView addSubview: _pIdLabel];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSString *textToMesure = _pIdLabel.text;
    
    NSDictionary *attributes = @{ NSFontAttributeName : _pIdLabel.font};
//      vai
//    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                               _pIdLabel.font, NSFontAttributeName
//                               , nil];
    


    CGSize s = [textToMesure sizeWithAttributes: attributes ];

    // iecentrējam pa y asi
    int pixelsToAdd = 10;
    CGRect frame = _pIdLabel.frame;
    frame.origin.y = self.contentView.frame.size.height / 2 - (s.height + pixelsToAdd)/2;
    frame.size.height = s.height + pixelsToAdd;
    [_pIdLabel setFrame: frame];
    //
}

#pragma mark public Metdohds

- (void)updateCellWithModel:(JTUser *)userModel
{
    _pUser = userModel;
    NSString *uIdString = [NSString stringWithFormat: @"%i", [_pUser.uId intValue]];
    [_pIdLabel setText: uIdString];
    
    NSString *name = [_pUser cleanName];
    NSString *email = _pUser.email;
    
    [self.textLabel setText: name];
    [self.detailTextLabel setText: email];
    
    
}

@end
