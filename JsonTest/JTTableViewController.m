//
//  JTTableViewController.m
//  JsonTest
//
//  Created by Ansis on 21/08/14.
//  Copyright (c) 2014 test. All rights reserved.
//

#import "JTTableViewController.h"

//models
#import "JTUser.h"

//views
#import "JTUserTableViewCell.h"

@interface JTTableViewController ()

@property (nonatomic, strong) NSArray *pAllUsers;

@end

@implementation JTTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setTitle: @"My Users"];
    [self p_loadUsersFromWeb];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _pAllUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = @"cell";
    JTUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellId];
    if (!cell){
        cell = [[JTUserTableViewCell alloc] initWithStyle: UITableViewCellStyleSubtitle reuseIdentifier: cellId];
    }
    
    JTUser *aUser = _pAllUsers[indexPath.row];
    
    [cell updateCellWithModel: aUser];
    
    return cell;
}


#pragma mark PRIVATE METHODS

- (void)p_loadUsersFromWeb
{
 
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        // izpildās bg thredā
        NSString *urlString = @"http://ayshareloc.com/assets/mock.json";
        NSURL *url = [NSURL URLWithString: urlString];
        
        NSString *jsonString = [NSString stringWithContentsOfURL: url encoding: NSUTF8StringEncoding error: nil];
        
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSMutableArray *tmpArray = [NSMutableArray new];
        for (NSDictionary *d in jsonArray){
            JTUser *aUser = [[JTUser alloc] initWithDictionary: d];
            [tmpArray addObject: aUser];
        }

        _pAllUsers = [NSArray arrayWithArray: tmpArray];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // izpildās MAIN / UI thredā
            // visas darbības, kas skar ui elementus jāveic main thredā
            [self.tableView reloadData];
        });
    });

    

    
}

@end
